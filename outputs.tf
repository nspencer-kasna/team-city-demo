output "external-ip-team-city-vm" {
  value = google_compute_instance.team-city-vm.network_interface.0.access_config.0.nat_ip
}