variable "gcp_instance_name" {
  description = "Instance name"
  type        = string
  default     = "team-city-vm"
}

variable "gcp_project" {
  description = "The project ID where all resources will be launched."
  type        = string
  default     = ""
}

variable "gcp_zone" {
  description = "The project ID where all resources will be launched."
  type        = string
  default     = "us-central1-a"
}

variable "gcp_region" {
  description = "The project ID where all resources will be launched."
  type        = string
  default     = "us-central1"
}
