resource "google_compute_disk" "team-city-disk" {
  name  = "team-city-boot-disk"
  zone  = var.gcp_zone
  image = "ubuntu-2004-focal-v20230724"
  size  = 20
}

resource "google_compute_instance" "team-city-vm" {
  name         = var.gcp_instance_name
  project      = var.gcp_project
  machine_type = "e2-medium"
  zone         = var.gcp_zone

  tags = ["http-server"]

  boot_disk {
    source = google_compute_disk.team-city-disk.name
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  metadata_startup_script = <<-EOF
    sudo install -m 0755 -d /etc/apt/keyrings 
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg 
    sudo chmod a+r /etc/apt/keyrings/docker.gpg 
    echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    docker run --name teamcity-server \
    -u root \
    -v team_city_data:/data/teamcity_server/datadir \
    -v team_city_logs:/opt/teamcity/logs  \
    -p 80:8111 \
    -d jetbrains/teamcity-server
    sleep 70
    docker run -it -d -e SERVER_URL="http://$(curl -s ifconfig.me):80" \
    --name teamcity-agent-with-docker \
    --link teamcity-server \
    -u root \
    -v team_city_agent_config:/teamcity_agent/conf \
    -v /var/run/docker.sock:/var/run/docker.sock  \
    -v /opt/buildagent/work:/opt/buildagent/work \
    -v /opt/buildagent/temp:/opt/buildagent/temp \
    -v /opt/buildagent/tools:/opt/buildagent/tools \
    -v /opt/buildagent/plugins:/opt/buildagent/plugins \
    -v /opt/buildagent/system:/opt/buildagent/system \
    -d jetbrains/teamcity-agent
    EOF
}

